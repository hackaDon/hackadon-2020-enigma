<?php
	// Connexion à la base
	include("db_connect.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	function getArticles()
	{
		global $conn;
		$query = "SELECT * FROM article";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
	
		json_encode($response, JSON_PRETTY_PRINT);
		var_dump($response);
	}
	
	function getArticle($id=0)
	{
		global $conn;
		$query = "SELECT * FROM article";
		if($id != 0)
		{
			$query .= " WHERE id=".$id." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function AddArticle()
	{
		global $conn;
		$title = $_POST["title"];
		$chapo = $_POST["chapô"];
		$text = $_POST["text"];
		$created = date('Y-m-d H:i:s');
		$modified = date('Y-m-d H:i:s');
		echo $query="INSERT INTO article(title, chapô, text, created, modified) VALUES('".$title."', '".$chapo."', '".$text."', '".$created."', '".$modified."')";
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'article ajout� avec succ�s.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'ERREUR!.'. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function updateArticle($id)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$title = $_PUT["title"];
		$chapo = $_PUT["chapô"];
		$text = $_PUT["text"];
		$created = 'NULL';
		$modified = date('Y-m-d H:i:s');
		$query="UPDATE article SET title='".$title."', chapo='".$chapo."', text='".$text."', modified='".$modified."' WHERE id=".$id;
		
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'article mis a jour avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de article. '. mysqli_error($conn)
			);
			
		}
		
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function deleteArticle($id)
	{
		global $conn;
		$query = "DELETE FROM article WHERE id=".$id;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'article supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression article a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	switch($request_method)
	{
		
		case 'GET':
			// Retrive Articles
			if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				getArticle($id);
			}
			else
			{
				getArticles();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;
			
		case 'POST':
			// Ajouter un article
			AddArticle();
			break;
			
		case 'PUT':
			// Modifier un article
			$id = intval($_GET["id"]);
			updateArticle($id);
			break;
			
		case 'DELETE':
			// Supprimer un article
			$id = intval($_GET["id"]);
			deleteArticle($id);
			break;

	}
?>